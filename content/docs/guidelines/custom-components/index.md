---
title: "Custom Components"
description: >
  The guidelines for creating custom components for Bizzflow.
lead: >
  The guidelines for creating custom components for Bizzflow.
date: 2022-01-20T11:48:00+01:00
lastmod: 2022-01-20T11:48:00+01:00
draft: false
images: []
menu:
  docs:
    parent: "guidelines"
weight: 10
toc: true
---

## Input and Output

If your component expects an input (this is mostly the case with transformations), it will always be in `/data/in/tables` directory within your component's container. For each table
you requested (using `in_tables` and `in_kexes` in configuration) there will be a CSV file.

If your components creates an output and you want it to be interpreted as a table in Bizzflow, put is as a CSV file to directory `/data/out/tables` within your component's container. After a successful run of your custom component, Bizzflow will find all CSV files in this directory and import it to the analytical warehouse.

The only requirement here is to be sensible about the file names (some special characters and whitespaces will be converted to underscores `_` for you) and to conform with the `UNIX CSV Dialect`.

## UNIX CSV Dialect

Bizzflow always expects all CSVs to be in UNIX CSV dialect.
UNIX CSV dialect comes from [Python's definition](https://docs.python.org/3/library/csv.html#csv.unix_dialect) and its configuration looks like this:

|                  |                     |      |
| ---------------- | ------------------- | ---- |
| line terminator  | LF                  | `\n` |
| field separator  | comma               | `,`  |
| field quote      | double quotes       | `"`  |
| quoting          | quote all fields    |      |
| escape character | backslash           | `\`  |
| quote escape     | double-double quote | `""` |
| header           | first line          |      |

### Example.csv

Following file conforms with the UNIX CSV dialect. Note that even the numeric fields are all quoted.

```csv
"order_id","customer","value"
"1","Carl","172.31"
"2","Jason ""The Piledriver"" Mason","999.2"
```
