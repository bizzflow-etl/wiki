---
title: "Naming conventions"
description: >
  These guidelines should make it clearer on how to name stuff when using or developing for Bizzflow.
lead: >
  These guidelines should make it clearer on how to name stuff when using or developing for Bizzflow.
date: 2023-03-28T15:22:02+02:00
lastmod: 2023-03-28T15:22:02+02:00
draft: false
images: []
menu:
  docs:
    parent: "guidelines"
weight: 20
toc: true
---

Throughout the documentation, you will find us refering to "sane names" and "sluggish" names. This page should make it
clearer on how to name stuff when using or developing for Bizzflow.

## How to be sane when naming things

Although there are some components that are even more strict about naming, the good rule of thumb is to use only
alphanumeric characters and underscores (`_`) or dashes (`-`) in your names. Make sure to always start with a letter.
This is because Bizzflow uses these names in various places (such as DAG ids, file names, etc.) and we want to make
sure that they are always valid.

This applies to all file names in your Bizzflow project structure, all values for `id` attributes and also for all
table names, kex names and column names in your analytical warehouse. Some warehouses are a little bit less strict
about the naming of the columns, but we still recommend to adhere to the same naming conventions.

## DAG IDs

Everytime you assign an ID to a Bizzflow configuration (such as filename for an extractor, or `id` attribute for
a transformation), this ID is used to generate a DAG iID and a task ID within another DAG. Because Airflow has
a limit od 64 characters for IDs and Bizzflow adds a component type prefix to every DAG (such as `00_Orchestration_`).
Saying all that, please note that 64 characters is more than it may seem and even names like
`this_may_look_very_long_but_it_is_actually_not` are perfectly fine.
