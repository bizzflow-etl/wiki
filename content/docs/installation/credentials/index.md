---
title: "Credentials"
description: >
  In order for Bizzflow Setup to work, it needs an access to your cloud project container.
  In this guide, you will learn how to obtain credentials to be used with the Bizzflow Setup.
  Start by selecting your cloud provider.
lead: >
  In order for Bizzflow Setup to work, it needs an access to your cloud project container.
  In this guide, you will learn how to obtain credentials to be used with the Bizzflow Setup.
  Start by selecting your cloud provider.
date: 2021-05-18T13:01:49+02:00
lastmod: 2021-05-18T13:01:49+02:00
draft: false
images: []
menu:
  docs:
    parent: "installation"
weight: 20
toc: false
---

{{<toggle id="gcp">}}

## Google Cloud Platform (GCP)

{{</toggle>}}

{{<collapse id="gcp">}}

In your GCP project, select `IAM & Admin` &rarr; `Service Accounts`.

{{<img src="gcp01.png" alt="IAM &amp; Admin" caption="IAM &amp; Admin">}}

Proceed by clicking `Create Service Account` and fill in service account's name (you may select any name that
fits your naming conventions). Confirm the settings by clicking `Create`.

{{<img src="gcp02.png" alt="Service account settings" caption="Service account settings">}}

Grant the service account following three roles:

- `Editor`
- `Security Admin`
- `Service Networking Admin`

and confirm service account settings by clicking `Done`.

{{<img src="gcp03.png" alt="Service account roles" caption="Service account roles">}}

You should then be transfered to the list of all service accounts in your project. Find our newly created one
in the list, click the three dots menu at the end of the line and select `Manage Keys`.

[{{<img src="gcp04.png" alt="Manage Keys" caption="Manage Keys">}}](/images/gcp04.png)

Click `ADD KEY` &rarr; `Create new key` on the next screen and confirm `JSON` key creation by clicking `CREATE`.

A `JSON` file should be downloaded to your PC. Anyone holding this file has an access to your GCP project,
do not ever share this file's contents with anyone you don't trust.

### APIs

Enable:
- cloud resource manager API <a href="https://console.cloud.google.com/marketplace/product/google/cloudresourcemanager.googleapis.com" target=_blank>here</a>
- compute engine API <a href="https://console.developers.google.com/apis/library/compute.googleapis.com" target=_blank>here</a>

{{</collapse>}}

{{<toggle id="aws">}}

## Amazon Web Services (AWS)

{{</toggle>}}

{{<collapse id="aws">}}

Login to your AWS account and select `IAM` service. Select `Users` in the left menu and then click `Add User` button.

{{<img src="aws01.png" alt="IAM" caption="IAM">}}

Pick any user name that fits your conventions, select `Programmatic access` in `Access type` and click `Next`.

{{<img src="aws02.png" alt="User creation" caption="User Creation">}}

In the permissions tab, select `Attach existing policies directly` and pick `AdministratorAccess` from the policies
list.

{{<img src="aws03.png" alt="Attach required policies" caption="Attach required policies">}}

{{<alert icon="☝">}}
We understand that giving **AdministratorAccess** is a little bit too open. We will update this part
of documentation ASAP with a final list of policies **actually** needed for the setup to succeed.
{{</alert>}}

Click `Next`, add any tags you would like to, then click `Next` again and confirm user creation with `Create user`
button.

{{<img src="aws04.png" alt="Create user" caption="Create user">}}

On the next page, take note of both your `Access Key ID` and `Secret Access Key` as you will need both
of those during Bizzflow's installation.

{{<img src="aws05.png" alt="AWS User Credentials" caption="AWS User Credentials">}}

{{</collapse>}}

{{<toggle id="azure">}}

## Microsoft Azure

{{</toggle>}}

{{<collapse id="azure">}}

### Ensure you are logged in the correct directory

Your subscription name should appear in the top right corner of the screen after you log in to
[Azure Portal](https://portal.azure.com).

![/images/azure/sp/01.png](/images/azure/sp/01.png)

If the subscription is not correct, click your account information and select `Switch directory`.

![/images/azure/sp/02.png](/images/azure/sp/02.png)

From the directory list select the subsription you wish to use for the installation.

![/images/azure/sp/03.png](/images/azure/sp/03.png)

### Create a service principal

The easiest way to create a service principal for RBAC (Role-Based Access Control) in Azure is using the command line.

Click on the command line icon next to your account information.

![/images/azure/sp/04.png](/images/azure/sp/04.png)

After some loading, a console window should appear at the bottom of the screen. Type:

```console
az ad sp create-for-rbac -n "BizzflowInstaller"
```

And hit Enter. Following information should appear in the console window:

![/images/azure/sp/05.png](/images/azure/sp/05.png)

Take note of your `appId`, `password` and `tenant` and store them securely. We will need them during the installation.
After storing the keys, you may close the console window.

### Assign policies to the service principal

Type `Subscriptions` in the search bar in the top of the screen and select `Subscriptions` resource.

![/images/azure/sp/06.png](/images/azure/sp/06.png)

From the list of subscriptions, select your designated subscription.

![/images/azure/sp/07.png](/images/azure/sp/07.png)

In the left menu, select `Access Control (IAM)`.

![/images/azure/sp/08.png](/images/azure/sp/08.png)

On the next screen, click on `Add role assignments`.

![/images/azure/sp/09.png](/images/azure/sp/09.png)

Select `Contributor` in the `Role` list, `User, group or service principal` in `Assign access to` and type
`BizzflowInstaller` into the `Select` text field. Our service principal should appear. Click it and confirm with
`Save` button in the bottom.

![/images/azure/sp/10.png](/images/azure/sp/10.png)
![/images/azure/sp/11.png](/images/azure/sp/11.png)

Repeat previous step, but select `User Access Administrator` as the `Role` this time.

![/images/azure/sp/14.png](/images/azure/sp/14.png)

### Confirm the assigned policies

To make sure we did everything right, click `Role Assignments` in the top menu in `Access Control (IAM)` section.

![/images/azure/sp/12.png](/images/azure/sp/12.png)

You should see our service principal twice in the list - as a `Contributor` and as `User Access Administrator`.

![/images/azure/sp/13.png](/images/azure/sp/13.png)

{{</collapse>}}

## Ready to roll?

If you have your installation credentials ready, let's take the next step to installation.
