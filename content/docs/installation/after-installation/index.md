---
title: "After Installation"
description: "These are the steps you probably should take right after installing your Bizzflow instance."
lead: "These are the steps you probably should take right after installing your Bizzflow instance."
date: 2021-05-19T23:07:08+02:00
lastmod: 2021-05-19T23:07:08+02:00
draft: false
images: []
menu:
  docs:
    parent: "installation"
weight: 40
toc: true
---

## Test everything works

To see everything went well, you should:

### Check the log and download the artifacts

You should receive a link to the installation job log via e-mail to the notification e-mail specified
in the setup guide. The log should say whether the job has **failed** or **succeeded**. If the job
failed and the log does not make any sense to you, please check our
[Troubleshooting]({{<ref "docs/troubleshooting/foreword">}}) section.

If your installation succeeded, your first action should be downloading the setup artifacts,
as **the artifacts will only be available for download for one week**. After this period,
they will be deleted.

**If you do not download the artifacts within the week, you will no be able to access your
Airflow instance for maintenance and destroy the resources created by the installer.**

{{<alert icon="❗">}}
Let us remind you again that **the setup artifacts contain sensitive data**, such as SSH keys
and passwords and should **never be shared with anyone**.
{{</alert>}}

The combination of `.tfvars` and `tfstate` files can be used to alter, repair or **completely destroy**
your Bizzflow installation along with all cloud resources. Please consult
[Terraform official documentation](https://www.terraform.io/docs/cli/commands/) on how to use them.

### Visit the Airflow UI

On the log page, you should find the IP address that can be used to visit your Bizzflow's UI. Simply copy
this IP address to your browser and Airflow webserver should appear. If you didn't specify differently
during installation, the default credentials should be:

```plain
user:     bizzflow
password: wolfzzib
```

This leads us to another step...

## Change the default user's password

In order to secure your Bizzflow instance, you should reset your password right after the installation.
In Airflow, log in (if you haven't already) and click `Bizzflow Administrator` in the top right corner of the screen
and select `Profile`. Then find the button `Reset my password` and click it. Follow instructions to change
the password.

## Run a DAG

If you want to check your Bizzflow really works, you may try running the demo extractor
`20_Extractor_ex-mysql_demo-classicmodels`. After it has finished, check the Storage Console by visiting
`http://{your-airflow-ip}/storage` in the browser. You should see two kexes were created by the extractor task.

## Learn the ways of the... Bizzflow

{{<img src="master.jpg" alt="you are on this council but we do not grant you the rank of master" caption="Listen to the Bizzflow guide, young Padawan">}}

You may already feel the Force (Bizz)flowing through you, but there are still things you need to learn before
you become the true Data Master. You need to learn to control your anger towards dirty data and
embrace your fears of failing orchestrations in order to bring balance to... the Force. Sorry, I just ran out of puns.

Be sure to check out the [Project Reference]({{<ref "docs/project-reference/project-structure">}}) and
our [Basic Tutorial]({{<ref "docs/basic-tutorial/environment">}}).
