---
title: "Install Bizzflow"
description: "If you have your credentials ready, let's install your own Bizzflow instance."
lead: "If you have your credentials ready, let's install your own Bizzflow instance."
date: 2021-05-19T11:28:01+02:00
lastmod: 2021-05-19T11:28:01+02:00
draft: false
images: []
menu:
  docs:
    parent: "installation"
weight: 30
toc: true
---

## Setup

Visit <a href=https://setup.bizzflow.app target="_blank">setup.bizzflow.app</a> to start the installation guide.
On the first screen, select your cloud provider and click `Next`.

All of the guide's steps are self-explanatory, but if you have any trouble understanding it,
you may continue reading here. If you encounter any issues and none of the guides answer your questions, please, reach out to us at [info@bizztreat.com](info@bizztreat.com).

## Credentials setup

Based on the provider you chose, fill in the credentials you obtained by following the
[previous guide]({{<ref "docs/installation/credentials">}}).

### GCP Credentials

`Service account key` is the content of the JSON key file downloaded from GCP's `IAM & Admin`.
When selecting `Region`, it is always a good practice to select one that is closest to you (or closest to
the person who will access Bizzflow regurarly).

{{<alert icon="❗">}}

**If you do not see the region you prefer**, let us know and we will consider adding it to the
list of supported regions.

{{</alert>}}

After filling in both `Service account key` and `Region`, click `Load Projects` to populate the combo
box labeled `Project` below.

From the list of projects, select the one you want to install Bizzflow into (in most cases, you will
only see one project).

Click `Next` and wait for the guide to validate your credentials and make sure you have the correct access
roles set up.

### AWS Credentials

Both `AWS Access Key ID` and `AWS Secret Access Key` can be found in AWS' `IAM` service when creating
the credentials according to the [previous chapter]({{<ref "docs/installation/credentials">}}).

In the `Region` combo box, pick the one that is closest to you or to whoever will be using
Bizzflow regurarly.

{{<alert icon="❗">}}

**If you do not see the region you prefer**, let us know and we will consider adding it to the
list of supported regions.

{{</alert>}}

Click `Next` and wait for the guide to validate your credentials and make sure you have the correct access
policies set up.

### Azure Credentials

All of `Client ID`, `Tenant` and `Client secret` can be found when creating `Service principal for RBAC`
in the [previous chapter]({{<ref "docs/installation/credentials">}}).

In the `Region` combo box, pick the one that is closest to you or to whoever will be using
Bizzflow regurarly.

{{<alert icon="❗">}}

**If you do not see the region you prefer**, let us know and we will consider adding it to the
list of supported regions.

{{</alert>}}

Click `Load Subscriptions`. If the credentials are valid, all Subscriptions that the credentials are valid for
will appear in the `Subscription` combo box. Pick the one you want to install Bizzflow in. Once you select
a subscription, `Resource group` combo box should get populated by resource groups within the subscription.
You can either let the guide create a Resource group for you (by selecting `<create new>`) or pick the one
you would like Bizzflow to reside within.

{{<alert icon="❗">}}
Please note, that it is usually a good idea to keep your resources separate from each other. **We always
recommend letting the guide create a new resource group** instead of picking an existing one.

This may save you some pain caused by naming conflicts.

{{</alert>}}

Click `Next` and wait for the guide to validate your credentials and make sure you have the correct role
assignments set up.

### Snowflake Credentials

If you picked `Snowflake` to be your data warehouse with
a [supported cloud provider]({{<ref "docs/getting-started/supported-environments">}}), you will be prompted
to provide the guide with your `Snowflake` credentials.

Follow the labels and notes in the form to fill in the correct data. The guide will create roles and users
needed for Bizzflow to work during the setup. You may create a specialized user for the purpose of the installation,
grant them the `SECURITYADMIN` role and delete the user after the Bizzflow setup is complete.

## Repository information

After you have set up all necessary credentials, you will be prompted to provide git repository information.
Currently, only SSH key authentication is supported with Bizzflow.

In the first text input, fill in the `repository remote`. With most git hosts you can find it on the repository
page. If not, it can be guessed, in most cases it will look like `git@host:path/to/repo.git`, e.g.
`git@github.com:tomasvotava/my-bizzflow-project.git`.

To generate a private SSH key, you may use this command:

```console
ssh-keygen -t RSA -m PEM -b 4096 -C 'bizzflow key'
```

{{<alert icon="❗">}}
Please, use passwordless SSH keys for Bizzflow. In order to automate Bizzflow's
access to your git repository, **the key must _not_ be protected with passphrase**.
{{</alert>}}

Do not forget to register the public part of the key to your git host, you may find the most common git hosts'
guides here:

- [gitlab.com](https://docs.gitlab.com/ee/ssh/#add-an-ssh-key-to-your-gitlab-account)
- [github.com](https://docs.github.com/en/github/authenticating-to-github/connecting-to-github-with-ssh/adding-a-new-ssh-key-to-your-github-account)
- [bitbucket.org](https://support.atlassian.com/bitbucket-cloud/docs/set-up-an-ssh-key/)

After registering the SSH key with your git host, paste the private part of the key pair to the form field
called `Private SSH key`.

After clicking `Next`, the guide will check whether the key and the repository is set correctly.

{{<alert icon="☝">}}
Although any repository would work, it is recommended to **use an empty repository for Bizzflow installation**.
{{</alert>}}

{{<alert icon="🤓">}}
When **migrating an existing Bizzflow project**, rename your existing _project.json_ (or _project.yaml_) file
to _project.json.bak_ (or _project.yaml.bak_ respectively) in the repository before starting installation.
This way the guide will generate a new project configuration file with correct environment settings
and you will be able to reflect your specific alterations to the file afterwards.
{{</alert>}}

## Notifications settings

The notifications settings configure the way Bizzflow will tell you that something is wrong. You may omit
this by selecting `No notifications`, but **we strongly discourage you from turning off the notifications**.

The prefered way is to fill in your SMTP configuration. After clicking `Next`, a test notification will
be sent using the SMTP configuration from the `E-mail from` address to `Notification e-mail` address.
If the attempt to send an e-mail fails, you will not be able to continue until the SMTP configuration
is valid.

{{<alert icon="❗">}}
Note that some SMTP servers only allow specific addresses to be used as from / to addresses.
Please consult your e-mail provider's documentation in order to properly setup the notifications.
What tends to work in most cases is sending the notifications from your e-mail to yourself, i.e.
setting both **E-mail from** and **Notification e-mail** to the same address.
{{</alert>}}

## _Click install&#8230;_

Before starting the installation, you may check all the details you provided in the previous steps.
If everything checks out, click `Install`. You will be redirected to a page containing log of the
installation, where you may monitor the progress.

**Leaving the page will not stop the installation**. We recommend bookmarking the page though, as the
url contains a token that may be used to access the log in the future.

{{<alert icon="❗">}}
**Please know, that the token in the url serves to download the installation artifacts as well.
The installation artifacts contain sensitive information, such as SSH keys and passwords,
and should _never be shared with anyone_**.
{{</alert>}}
