---
title: "Manage User Access"
description: >
  If you ever come into a situation where you will need to include more users in your Bizzflow project, these
  are steps and recommendations for best practices in collaboration.
lead: >
  If you ever come into a situation where you will need to include more users in your Bizzflow project, these
  are steps and recommendations for best practices in collaboration.
date: 2021-05-24T09:29:50+02:00
lastmod: 2021-05-24T09:29:50+02:00
draft: false
images: []
menu:
  docs:
    parent: "administration-and-maintenance"
weight: 20
toc: true
---

The method you would probably think about first is sharing your `bizzflow` user's password with whoever you want
to share your project with. **Please, don't do that.** By sharing your Bizzflow credentials, you completely
loose control of who has access to your project, who does what (audit) and using
[sandboxes]({{<ref "docs/administration-and-maintenance/manage-sandbox">}}) will become impossible.

## Create User

### Manage access to Airflow UI and Flow UI

The clean way of doing that is creating a new user in your Airflow UI. Bizzflow will automatically create
Flow UI access for any user that exists in Airflow.

In your Airflow UI, click `Security` and then `List Users`. There is a plus `+` button in the topleft corner
of the form that appears in the page. Fill in your new user's information.

If you want the user to be able to "do anything", feel free to assign him `Admin` role. See
[this article](https://airflow.apache.org/docs/apache-airflow/1.10.15/security.html#default-roles) official Apache
Airflow documentation for more information on what the roles mean.

{{<alert icon="❗">}}
Please note that every user with Airflow UI access has Flow UI access automatically.
**Every user you add to the project may display, preview and delete data from your analytical warehouse (storage)**.
{{</alert>}}

### Manage access to the project's repository

If you want the user to be able to create transformations, setup extractor configurations and all other actions
described in [Project Reference]({{<ref "docs/project-reference/project-structure">}}), you need to make
sure they have access to your project's repository.

This process differs from one git host to other. These are the guides for the most commonly used git hosts:

- [Gitlab](https://docs.gitlab.com/ee/user/project/members/#add-users-to-a-project)
- [Github](https://docs.github.com/en/github/setting-up-and-managing-your-github-user-account/managing-access-to-your-personal-repositories/inviting-collaborators-to-a-personal-repository)
- [Bitbucket](https://support.atlassian.com/bitbucket-cloud/docs/grant-repository-access-to-users-and-groups/)

## Remove User

In order to completely remove a user, these are the steps you should take.

### Remove user from Airflow UI and Flow UI

In Airflow UI click `Security` and `List Users`. Click eraser icon next to the user you would like to remove
and confirm the action.

This does not remove user's sandbox and their database user from the warehouse, though. This cannot be achieved
without direct access to the warehouse, for now. The recommended way is deleting the sandbox kex using
[Storage Console]({{<ref "docs/administration-and-maintenance/manage-storage">}}).

### Remove user from the project's repository

Again, this differs from provider to provider. Follow your provider's guide to remove the user from your repository.
