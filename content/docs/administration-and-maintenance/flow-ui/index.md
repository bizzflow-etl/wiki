---
title: "Flow UI"
description: >
  Although Airflow is a mighty tool, it imposes some restrictions on us. That's why we created Flow UI.
lead: >
  Although Airflow is a mighty tool, it imposes some restrictions on us. That's why we created Flow UI.
date: 2022-11-19T11:00:00+01:00
lastmod: 2021-05-24T10:04:56+02:00
draft: false
images: []
menu:
  docs:
    parent: "administration-and-maintenance"
weight: 30
toc: true
---

## What is Flow UI?

For easier life of a Bizzflow user, we developed **Flow UI `(beta)`**.
Flow UI is a web frontend that enables you to view and manage resources within your Bizzflow project.
Resources users would be unable to find in Airflow, such as Data Warehouse Storage, Vault secrets and Sandboxes, have their own views within Flow UI.

## Reaching Flow UI

You can either append `/flow` to your Airflow URL as you see it in the browser, or click `Consoles -> Flow UI` in Airflow to find yourself in the Flow UI.

{{<img src="flow-ui-link.png" alt="Reaching Flow UI" class="border-0" caption="Reaching Flow UI">}}

## Authentication &amp; Authorization

Flow UI uses your login from Airflow. Any user with an active Airflow account
will be able to reach Flow UI, manage storage, load themself a sandbox
and display visible credentials within the vault
(see in Credentials Management
[which credentials are visible]({{<ref "docs/administration-and-maintenance/credentials-management#visible-credentials">}})).

## Basic usage

Please see individual pages.

- [Manage Storage]({{<ref "docs/administration-and-maintenance/manage-storage">}})
  - view tables and kexes within your analytical warehouse
- [Manage Sandbox]({{<ref "docs/administration-and-maintenance/manage-sandbox">}})
  - create and manage development area for your SQL transformations
- [Credentials Management]({{<ref "docs/administration-and-maintenance/credentials-management">}})
  - a secure way to store and access your secrets within Flow UI Vault
