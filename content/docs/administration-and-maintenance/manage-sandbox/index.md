---
title: "Manage Sandbox"
description: "Sandbox is an environment every user may utilize to develop transformations without interference with the production data."
lead: "Sandbox is an environment every user may utilize to develop transformations without interference with the production data."
date: 2021-05-24T08:58:56+02:00
lastmod: 2022-11-25T08:58:56+02:00
draft: false
images: []
menu:
  docs:
    parent: "administration-and-maintenance"
weight: 50
toc: true
---

Every Airflow User can load their own sandbox through [Flow UI]({{<ref "docs/administration-and-maintenance/flow-ui">}}).
Sandbox is a kex you can use to develop within. Use any database tool (such as [DBeaver](https://dbeaver.io/)
or [DataGrip](https://www.jetbrains.com/datagrip/)) to connect to the sandbox and run any SQL queries.
You don't have to be especially careful, too - **queries you run on the sandbox can in no way interact with
the rest of the warehouse**.

{{<img src="flow-ui-sandbox.png" alt="Flow UI Sandbox" caption="Sandbox" class="border-0">}}

## Usage

All of the fields are pretty self-explanatory. You may load a transformation's input into the sandbox and
even run the transformation's queries to make sure they are written correctly and then connect to the sandbox
and preview the tables created using your transformation's queries to check the output.

You may also choose to select arbitrary tables and kexes from the storage by selecting `➕ Additional tables`.

If you check `Clean sandbox (drop all tables)`, Bizzflow will ensure your sandbox is empty before doing anything.

By pushing `Load sandbox` button, DAG within Airflow will fire. You can then follow the job in Airflow UI.

## Credentials

If you want to connect to the sandbox, you will need credentials. Credentials to your sandbox are automatically generated with the first load of your sandbox and you may find them in the
[Vault]({{<ref "docs/administration-and-maintenance/credentials-management">}}).

In the `Sandbox & Datamarts` section you can find your sandbox account's password. Only you can
view your sandbox password to make sure nobody can interact with your data, so keep you password safe.
