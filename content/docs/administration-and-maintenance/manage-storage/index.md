---
title: "Manage Storage"
description: ""
lead: ""
date: 2021-05-24T08:55:51+02:00
lastmod: 2021-05-24T08:55:51+02:00
draft: false
images: []
menu:
  docs:
    parent: "administration-and-maintenance"
weight: 40
toc: true
---
Storage management is a part of [Flow UI]({{<ref "docs/administration-and-maintenance/flow-ui">}}). You can access it
by clicking `Storage` in Flow UI's main navigation.

{{<img src="flowui-storage-page.png" alt="Flow UI - Storage page" class="border-0" caption="Storage page">}}

## Basic usage

In the Storage console, you can **browse kexes and tables**,
**preview, delete, truncate and copy tables** and **delete kexes**.

### Refresh storage

To minimize warehouse usage, we do not query the tables every time you
try to access them in the Storage console, but instead cache the storage
tree. If you want to see the most recent changes within the storage,
you have to either refresh the whole storage tree using
`Refresh Storage` button or refresh parts of the tree by either pushing
the `Refresh list of kexes` or `Refresh tables in <kex>` buttons.

### Kex actions

If you select a kex in the browser without selecting any of the tables,
you may open `Actions` menu to delete the kex.

{{<alert icon="❗">}}
Please note that deleting kex is irreversible.
{{</alert>}}

### Table actions

Once a table is selected, you are presented with table details, such as
its creation date, size and row count.

At the bottom of the page there is the table schema with column names
and their data types.

By clicking `Table preview` button, you can preview a 100 random rows
from the table.

{{<img src="flowui-storage-preview.png" caption="Flow UI Table preview" alt="Flow UI table preview" class="border-0">}}

In the `Actions` menu you may copy the table, truncate it (delete all
rows) or delete the table completely.

{{<img src="flowui-storage-table-actions.png" caption="Flow UI Table actions" alt="Flow UI table actions" class="border-0">}}

{{<alert icon="❗">}}
Please note that deleting table is irreversible.
{{</alert>}}

### Display `raw_` kexes

To display kexes with staging data from extractors, uncheck
`Only show standard kexes`.

- To create new Kex, click the `+ create kex` button.
- To explore a Kex, click the `+` sign in front of the Kex name.
- You can also explore individual table. Select the table by clicking on it. Then you can see the table `Summary`, `Preview`, or list of `Columns` under the corresponding tabs. You can also truncate or delete the table under `Actions`.
