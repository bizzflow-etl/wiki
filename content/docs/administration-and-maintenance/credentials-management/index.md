---
title: "Credentials Management"
description: "Vault provides you a with way to store sensitive data in your Bizzflow configuration securely."
lead: "Vault provides you a with way to store sensitive data in your Bizzflow configuration securely."
date: 2022-11-25T08:58:56+02:00
lastmod: 2022-11-25T08:58:56+02:00
draft: false
images: []
menu:
  docs:
    parent: "administration-and-maintenance"
weight: 60
toc: true
---

If you find yourself in a situation when you would want to put your credentials or secrets into a Bizzflow
configuration file (such as extractor database password or API token for a writer), you should use `Vault`
to store them and point to them in your configuration.

You will find the vault in [Flow UI]({{<ref "docs/administration-and-maintenance/flow-ui">}}).

{{<img src="flow-ui-vault.png" alt="Flow UI Vault" caption="Vault" class="border-0">}}

## Basic usage

You can switch between [credential types](#credential-types) using the tabs at the top of the Vault page.
After selecting a credential you will see a dialog. If the credential type is [visible](#visible-credentials),
you will see your credentials in plain text, otherwise you won't see its value, but you can type in a new
value and update it in the store.

With the `➕ Create new...` button, you may create a new credential in the vault.

{{<img src="flow-ui-vault-new-credentials.png" alt="Flow UI Vault add credentials" caption="Adding new credentials" class="border-0">}}

{{<alert icon="❗">}}
Remember to keep all your IDs [`sluggish`](https://en.wikipedia.org/wiki/Clean_URL).
See our [naming conventions]({{<ref "docs/guidelines/naming-conventions">}}) for reference.
{{</alert>}}

### Using credentials in configuration

If you need to include a secret value in your configuration, refer to your vault entry using its id with `#!#:`
prefix. Add `my-password` credential to the vault with value `this-is-my-password` and place it in your configuration
instead of your actual secret, such as this:

```json
{
  // ❌ This is wrong:
  "password": "this-is-my-password",

  // ✅ This is correct:
  "password": "#!#:my-password"
}
```

Or in `YAML`:

```yaml
# ❌ This is wrong:
password: "this-is-my-password"

# ✅ This is correct:
password: "#!#:my-password"
```

Bizzflow will automatically replace the link with the secret value when needed.

## Credential types

- **Project credentials**
  - Credentials you create and may use within your Bizzflow configuration files.
- **Sandbox &amp; Datamarts**
  - Credentials created by Bizzflow. See [Visible credentials](#visible-credentials) for more details.
- **Special Credentials**
  - Credentials Bizzflow needs for its internal use. Do not tamper with them, you may break your Bizzflow deployment.

## Visible credentials

Credentials listed in `Sandbox & Datamarts` section are visible. Datamart credentials (starting with `dm_`)
are **visible in plain text for all users**. Sandbox credentials (starting with `dev_`) are
**only visible in plain text for users they belong to**. Keep that in mind when adding users to your project.
