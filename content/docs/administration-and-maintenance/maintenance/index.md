---
title: "Maintenance"
description: >
  From time to time, some steps are needed to be taken to take care of your Bizzflow instance and keep it clean.
lead: >
  From time to time, some steps are needed to be taken to take care of your Bizzflow instance and keep it clean.
date: 2021-05-24T10:04:56+02:00
lastmod: 2021-05-24T10:04:56+02:00
draft: false
images: []
menu:
  docs:
    parent: "administration-and-maintenance"
weight: 10
toc: true
---

## Services

Services are docker containers running in your Airflow VM.

### Monitoring

In certain situations, your data may become unavailable for reasons other than incorrect SQL queries. It is, therefore, important to pay attention to your virtual machines and the services running on them. To address these concerns, we strongly recommend implementing monitoring for your cloud infrastructure. This way, you can receive timely notifications when, for example, your machines are running low on CPU power, indicating the need for scaling up or potentially optimizing your project design.

At Bizztreat, we prioritize setting up monitoring for our clients' installations as part of our managed license package. If you would like to learn more about our services, please feel free to contact us at [info@bizztreat.com](info@bizztreat.com).

### List of services

| Service name | Description | Required |
| ------------ | ----------- | -------- |
| `flow-ui` | Flow UI frontend | &#10005; |
| `flow-ui-backend` | Flow UI backend API | &#10005; |
| `airflow-webserver` | Airflow webserver | &#10003; |
| `airflow-scheduler` | Airflow scheduler | &#10003; |
| `celery-worker` | Background worker for Flow UI backend API | &#10005; |
| `redis` | redis in-memory cache for Flow UI backend API | &#10005; |
| `bizzflow-init` | Initialize Airflow metadata database and configuration, clone project from git, **running repeatedly may cause problems with encrypted credentials** | &#10005; |

### Restart services

When the Airflow UI is not responding, scheduler is not queuing tasks correctly or your consoles seem to be quirky,
your best bet is restarting the service that is misbehaving.

#### 1. SSH into the Airflow machine

Login using SSH key from the
[installation artifacts]({{<ref "docs/installation/after-installation#check-the-log-and-download-the-artifacts">}}).
The user name is always `bizzflow`.

#### 2. Restart required services

```console
docker-compose -p bizzflow restart
```

### Restart deployment

If restarting specific service(s) doesn't help, you may try deploying all services again.

#### 1. SSH into the Airflow machine

See [above](#1-ssh-into-the-airflow-machine).

#### 2. Destroy current deployment

```console
docker-compose -p bizzflow down
```

#### 3. Run deployment again

```console
docker-compose -p bizzflow --env-file project.env up -d flow-ui airflow-webserver
```

If your git repository containing Bizzflow project is not publicly accessible, you need to tell
Bizzflow to use an SSH key to authenticate with your git host. This is done using `GIT_SSH_KEY`
environmental variable. In most cases, the command above will look like this:

```console
GIT_SSH_KEY=`cat id_git` docker-compose -p bizzflow --env-file project.env up -d flow-ui airflow-webserver
```

### Get service's logs

Sometimes you need to know what is going wrong. In that case, your best bet is to look at the service's logs.

#### 1. SSH into the Airflow machine

See [above](#1-ssh-into-the-airflow-machine).

#### 2. Attach to logs live

```console
docker logs <service-name>_1
```

For service names see above - [list of services](#list-of-services).

Press `Ctrl+C` (or `Cmd+C` on Mac) to exit the logs.

## Disk cleanup

Sometimes you will run out of space on your Airflow VM. Following are some places you may look to clean up some space.

### Logs Cleanup

Airflow generates lots of logs. All of them are stored and not all of them are needed to kept.

#### 1. SSH into the Airflow machine

See [above](#1-ssh-into-the-airflow-machine).

#### 2. Cleanup the logs

```console
year=`date +'%Y'`
cmonth=`date +'%m'`
let lmonth=$cmonth-1
rm -rf ~/airflow/logs/scheduler/{2020..$year}-{01..`printf "%02d" $lmonth`}*
```

This will delete all logs since January 2020 until last month.

If you do not understand the curly braces, see [here](https://www.linuxjournal.com/content/bash-brace-expansion)
for more information.


## Bizzflow update

Ensuring the regular updates of Bizzflow is crucial as we continuously enhance its functionality and address any bugs. You can find details about the available versions in the [release notes]({{<ref "release-notes">}}).
Bizzflow is designed as a multi-container Docker application. Therefore, updating Bizzflow involves updating all the
associated containers. The process outlined below describes how to update a Bizzflow installation based on the provided
documentation. Note that if you have a custom deployment, the steps may vary. We recommend using Docker Compose for
running Bizzflow. You can locate the docker-compose.yaml file in the home directory of your Bizzflow server.
Alternatively, you may check the Bizzflow Git repository for any newer versions. To simplify the update process, we
utilize environment variables defined in the compose file. Thus, updating the .env file is all that's needed. Please
follow the step-by-step guide below for detailed instructions.

### Step by step guide

To update the Bizzflow application you would follow these steps:

1. Access the server or environment where the Bizzflow application is running.

2. Locate the `.env` file within the Bizzflow application directory.

3. Open the `.env` file using a text editor or a command-line text editor such as Nano or Vim.

4. Look for the variables related to the Docker images (BIZZFLOW_IMAGE, FLOW_UI_BACKEND_IMAGE, FLOW_UI_FRONTEND_IMAGE).

5. Update the values of these variables with the proper version or tag of the Docker images you want to use for the update. Ensure that you provide the correct version information to match the desired Docker images.

6. Save the changes to the `.env` file.

7. Use the following command to restart the application and apply the changes:

   ```console
   GIT_SSH_KEY=`cat id_git` docker-compose -p bizzflow up --remove-orphans -d airflow-webserver flow-ui
   ```

   This command will stop and restart the containers defined in your `docker-compose.yml` file, using the updated configuration from the `.env` file.

8. Allow some time for the application to restart and for the changes to take effect. You can monitor the application logs for any errors or unexpected behavior during the restart process.

