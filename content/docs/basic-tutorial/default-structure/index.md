---
title: "Bizzflow R2 Project Directory structure"
description: ""
lead: ""
date: 2021-05-14T08:48:57+00:00
lastmod: 2021-05-14T08:48:57+00:00
draft: true
images: []
menu:
  docs:
    parent: "basic-tutorial"
weight: 10
toc: true
---

## Project

```plain
| ~/project
├── extractors
|   └── zumpa-mysql.json
|   └── salesforce.json
├── transformations
│   └── some-sql-transformation
│   |   ├── 01_order_init.sql
│   |   └── 02_order_process.sql
│   └── some-py-transformation
│   |   └── main.py
├── orchestrations.json
└── transformations.json
└── step.json
└── datamarts.json
└── project.json
└── sandbox.json
└── .bizzflow_project
```

## orchestrations.json

```json
[
  {
    "id": "hlavni orchestrace",
    "schedule": "0 1 * * *",
    "tasks": [
      {
        "type": "extractor",
        "id": "zumpa-mysql",
        "continue_on_error": true
      },
      {
        "type": "transformation",
        "id": "main",
        "continue_on_error": false
      },
      {
        "type": "datamart",
        "id": "something",
        "continue_on_error": false
      }
    ]
  },
  {
    "id": "jina orchestrace",
    "tasks": []
  }
]
```

## transformations.json

```json
[
  {
    "type": "sql",
    "source": "some-sql-transformation",
    "id": "main",
    "in_kex": "in_something",
    "out_kex": "out_something",
    "input_tables": [],
    "query_timeout": 600,
    "transformation_service_account": ""
  },
  {
    "type": "python3",
    "source": "some-py-transformation",
    "id": "data-fucking-science",
    "in_kex": "in_something",
    "out_kex": "out_something",
    "input_tables": [],
    "query_timeout": 600,
    "transformation_service_account": ""
  }
]
```

## step.json

```json
{
  "whitelist": {
    "bizzflow-install-gco.raw_ex_mysql_ejtest.tables": {
      "columns": [
        "table_schema",
        "table_name",
        "engine",
        "version",
        "data_length",
        "__timestamp"
      ]
    }
  },
  "union": {
    "bizzflow-install-gco.raw_ex_mysql_ejtest.tables": {
      "sources": [
        {
          "kex": "raw_ex_mysql_ejtest",
          "table": "tables2",
          "project": "bizzflow-install-gco"
        }
      ],
      "distinct": false
    }
  },
  "filter": {
    "bizzflow-install-gco.raw_ex_mysql_ejtest.tables": {
      "columns": [
        {
          "column": "data_length",
          "condition": ">",
          "value": 0,
          "type": ""
        }
      ],
      "custom_query": "`data_length` > 0 AND `version` = '10'"
    }
  },
  "copy": {
    "bizzflow-install-gco.raw_ex_mysql_ejtest.tables": {
      "incremental": true,
      "primary_key": ["table_schema", "table_name"],
      "mark_deletes": false,
      "destination": {
        "kex": "in_ex_mysql_ejtest",
        "table": "tables",
        "project": "bizzflow-install-gco"
      }
    }
  }
}
```

## datamarts.json

```json
[
  {
    "id": "datamart1",
    "out_kex": "out_something",
    "dm_kex": "dm_something"
  },
  {
    "id": "datamart2",
    "out_kex": "out_something",
    "dm_kex": "dm_something"
  }
]
```

## project.json

```json
{
  "project_id": "",
  "git_project_path": "",
  "git_toolkit_path": "",
  "git_toolkit_tag": "",
  "dataset_location": "EU",
  "compute_zone": "",
  "compute_region": "",
  "notification_email": [],
  "debug": false,
  "live_bucket": "",
  "archive_bucket": "",
  "worker_machine": [
    {
      "name": "",
      "host": "",
      "user": "",
      "components_path": "/home/admin/components",
      "data_path": "/home/admin/data",
      "config_path": "/home/admin/config"
    }
  ],
  "user": "admin",
  "query_timeout": 600,
  "hostname": "",
  "public_ip": "",
  "classes": {
    "storage_manager": "BqStorageManager",
    "sandbox_manager": "BqSqlSandboxManager",
    "vault_manager": "AirflowVaultManager",
    "worker_manager": "GcpWorkerManager",
    "file_storage_manager": "GcsFileStorageManager",
    "datamart_manager": "BqDatamartManager",
    "credentials_manager": "GcpCredentialsManager",
    "transformation_executor": "BqSqlTransformationExecutor",
    "step": "BQSTep"
  }
}
```

## zumpa-mysql.json

```json
{
  "type": "ex-mysql",
  "config": {
    "host": "zumpa-vps.tk",
    "user": "zumpa",
    "password": "#!#:zumpaheslo"
  }
}
```

## sandbox.json

```json
[
  "mike.wazowski@yahoo.com",
  "carl.the.albino.crocodile@everywhere.com",
  "*@my-domain.com"
]
```

password ziskame pomoci `credentials_manager.get_credentials("zumpaheslo")`
