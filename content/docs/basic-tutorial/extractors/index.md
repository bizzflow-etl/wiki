---
title: "Extractors"
description: ""
lead: ""
date: 2021-05-14T08:48:57+00:00
lastmod: 2021-05-14T08:48:57+00:00
draft: false
images: []
menu:
  docs:
    parent: "basic-tutorial"
weight: 40
toc: true
tags:
  - extractor
---

As stated in the chapter [Project Design](project-design.md#extractor), extractors are components, that download
data from your data source. In this chapter, we will update our Bizzflow project so that it know how to extract
data from [Relational Dataset Repository](https://relational.fit.cvut.cz/) maintained by Faculty of Information
Technology at Czech Technical University in Prague. The dataset we are going to use is
[ClassicModels](https://relational.fit.cvut.cz/dataset/ClassicModels).

Right now, this is what our Airflow UI looks like:

{{<img src="airflow.png" alt="Airflow UI" class="border-0" caption="Airflow UI">}}

Our goal in this chapter is to add an extractor configuration, so that our Airflow UI looks like this:

{{<img src="airflow-extractor.png" alt="Airflow UI with extractor DAG" class="border-0" caption="Airflow UI with extractor DAG">}}

## Putting the information together

In the previous chapter [Project Design](project-design.md#extractor), we said that we need at least three
pieces of information about our data source in order to make Bizzflow extract data from it - **data source type**
**credentials** and **the source (source tables)**. Let's take a look at the way we can find this out.

### Data source type

First, we need to know, what is the database backend. From the description of the data source, we see that the
database lives within `MySQL` (`MariaDB`) server. Let's look up `MySQL` within
[Bizzflow extractors](https://gitlab.com/bizzflow-extractors) repository.

{{<img src="search-mysql.png" alt="Search for MySQL" class="border-0" caption="Search for MySQL">}}

There is only a single result - [`MySQL extractor`](https://gitlab.com/bizzflow-extractors/ex-mysql).

{{<img src="search-mysql-result.png" alt="Search result" class="border-0" caption="Search result">}}

Open the repository and take note of the repository name. You can find it at either at the end of the URL or
next to the branch name (as seen in the picture below).

{{<img src="mysql-component.png" alt="Component name" class="border-0" caption="Component name">}}

For the `MySQL extractor` it is `ex-mysql`. We will need it to tell Bizzflow which component to use when connecting
to our data source.

### Credentials

Luckily, we do not have to go to Hell and back for this as the maintainers of the Relational Dataset Repository
put the credentials in the bottom of
[the dataset page](https://relational.fit.cvut.cz/dataset/ClassicModels).

{{<img src="dataset-credentials.png" alt="Dataset credentials" class="border-0" caption="Dataset credentials">}}

### Source tables

For the purpose of this demo, let's download every single table, these are namely `orderdetails`, `orders`,
`payments`, `products`, `customers`, `productlines`, `employees` and `offices`.

## Creating the configuration

We have all the information we need, so let's put it all together.

Create a new empty file within `extractors/` directory in the Bizzflow project repository and name it
`classicmodels.json`. The structure of extractor configurations JSON files is fairly easy:

---

`extractors/classicmodels.json`

```json
{
  "type": "{component id}",
  "config": {
    /* Component-specific extractor configuration */
  }
}
```

We already know `component id` from
[`MySQL Extractor` component repository](https://gitlab.com/bizzflow-extractors/ex-mysql) - `ex-mysql`, so we may as
well fill it in:

---

`extractors/classicmodels.json`

```json
{
  "type": "ex-mysql",
  "config": {}
}
```

This way, Bizzflow will know to use `MySQL Extractor` when processing this data source.

For the component-specific config we need to take a look at
[the component repository](https://gitlab.com/bizzflow-extractors/ex-mysql) again and scroll down to the
example configuration:

{{<img src="mysql-example.png" alt="MySQL extractor component configuration example" class="border-0" caption="MySQL extractor component configuration example">}}

This is the object we will need to "nest" underneath the `config` key in our extractor configuration:

```json
{
  "$schema": "https://gitlab.com/bizzflow-extractors/ex-mysql/-/raw/master/config.schema.json",
  "user": "patrik",
  "password": "12345",
  "host": "mysql-db.cz",
  "database": "customers",
  "query": {
    "table_name": "SELECT * FROM table_name"
  }
}
```

{{<alert icon="☝">}}
The `$schema` key can be omitted in the configuration but it helps you,
because some IDEs use it to autocomplete JSON keys for you.
{{</alert>}}

All we need to do is fill in our real credentials we got in one of the previous steps, the configuration will
look like this:

```json
{
  "$schema": "https://gitlab.com/bizzflow-extractors/ex-mysql/-/raw/master/config.schema.json",
  "user": "guest",
  "password": "relational",
  "host": "relational.fit.cvut.cz",
  "database": "classicmodels",
  "query": {
    "orderdetails": "SELECT * FROM `orderdetails`",
    "orders": "SELECT * FROM `orders`",
    "payments": "SELECT * FROM `payments`",
    "products": "SELECT * FROM `products`",
    "customers": "SELECT * FROM `customers`",
    "productlines": "SELECT * FROM `productlines`",
    "employees": "SELECT * FROM `employees`",
    "offices": "SELECT * FROM `offices`"
  }
}
```

{{<alert icon="❗">}}
You should avoid using asterisk (`*`) in `SELECT` statements.
This way, if any of the database's columns change, you will
know it **before** the component actually tries to download
the data. Changes in the sources are best to be known as soon
as possible. For the sake of simplicity, we will use the `*` here.
In this case, do as we say, not as we do.
{{</alert>}}

**But wait**. I don't think it is a good idea to store password (even though a public one) in the file I am about
to commit to a repository (even though it may not be public). The thing is:

{{<alert icon="❗">}}
You should **never commit sensitive information like passwords to any repository**.
This is a silly thing to do that may lead to leaking sensitive information.
Even deleting the password afterwards has no effect because

a) the password may have already been copied
b) the password can still be accessed via git history

See [this article](https://www.honeybadger.io/blog/git-security/) for more information on this.
{{</alert>}}

The question remains: **how do we tell Bizzflow how to connect to the data source if we may not tell it
the password?**

Luckily, Bizzflow comes armed with a security measure just for this use-case. Open your
[Flow UI and select Vault]({{<ref "docs/administration-and-maintenance/credentials-management" >}}).

Click `➕ Create new...`, fill in `ID` and `Value` so that the id is something easily distinctible and relatable
to extractor configuration and value is our password `"relational"` and save it.

{{<img src="flow-ui-vault-form-filled.png" alt="Flow UI vault with credentials filled" class="border-0" caption="Flow UI Vault Credentials">}}

The way we tell Bizzflow to look into the connection instead of just plainly using the password is by using a special
hash string `#!#:{connection id}`, in our case this will be `#!#:classicmodels`.

If you now paste your configuration into our `config` key within the JSON file, it should look like this:

---

`extractors/classicmodels.json`

```json
{
  "type": "ex-mysql",
  "config": {
    "$schema": "https://gitlab.com/bizzflow-extractors/ex-mysql/-/raw/master/config.schema.json",
    "user": "guest",
    "password": "#!#:classicmodels",
    "host": "relational.fit.cvut.cz",
    "database": "classicmodels",
    "query": {
      "orderdetails": "SELECT * FROM `orderdetails`",
      "orders": "SELECT * FROM `orders`",
      "payments": "SELECT * FROM `payments`",
      "products": "SELECT * FROM `products`",
      "customers": "SELECT * FROM `customers`",
      "productlines": "SELECT * FROM `productlines`",
      "employees": "SELECT * FROM `employees`",
      "offices": "SELECT * FROM `offices`"
    }
  }
}
```

If you commit the changes and merge them to `master` branch of your Bizzflow project repository, you should be ready
to go along to your Airflow UI.

## Running the task

Wait. It still looks like this, doesn't it?

{{<img src="airflow.png" alt="Airflow UI" class="border-0" caption="Airflow UI">}}

We wanted to add the extractor configuration, how is it possible, that the DAG is not there yet?

That's right, we need to run `90_update_project` DAG first.

{{<alert icon="☝">}}
You see, there is a copy of your Project repository in your Airflow machine, but it does not get updated
automatically as you make changes to the repo. This is because we prefer running _last working configuration_
to running _lastest, top-notch, brand new configuration_. Remember this.
{{</alert>}}

Click on the play button `▶` (Trigger DAG) on the line of the `90_update_project` DAG and confirm running it with
`Trigger` button.

You should see a bright green circle on the `90_update_project` line:

{{<img src="airflow-running.png" alt="Airflow - Running task" class="border-0" caption="Airflow - Running task">}}

{{<alert icon="☝">}}
If you do not see the DAG running and after a while, your extractor configuration hasn't appear yet, please double check that your `90_update_project` is enabled - `On` should be at the begining of the DAG's line.
{{</alert>}}

Try refreshing the page after a while and you should now see the extractor DAG like this:

{{<img src="airflow-extractor.png" alt="Airflow with extractor DAG" class="border-0" caption="Airflow with extractor DAG">}}

> Again, the DAG will probably appear being `Off`, switch it to `On` right now.

Nothing should prevent us from running the component now. Click the play button `▶` (Trigger DAG) for our new
`20_Extractor_ex-mysql_classicmodels` DAG and confirm again by clicking `Trigger`. The extractor should now be
running. You can either check it via `Consoles -> Latest tasks` or on the main Airflow UI screen by the light green
circle. Once the circle disappears (or the task status in `Latest tasks` turns to `success`), everything
should be done.

{{<img src="airflow-task-running.png" alt="Airflow running task" class="border-0" caption="Airflow running task">}}

{{<img src="airflow-task-done.png" alt="Airflow comploted task" class="border-0" caption="Airflow comploted task">}}

## See the results

Go to [Flow UI's Storage page]({{<ref "docs/administration-and-maintenance/manage-storage">}}) and click
`Refresh Kexes`. Kex `in_ex_mysql_classicmodels` should appear. When you click the kex name,
you should see the tables that the kex contains.

{{<img src="flow-ui-storage-extractor.png" alt="Flow UI storage with extracted tables" class="border-0" caption="Flow UI storage with extracted tables">}}

## Well done

We have our extractor configuration, let's take a deep breath and transform the 💩 out of our data!
