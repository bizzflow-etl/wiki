---
title: "Naming"
description: "Naming recommendations and rules in Bizzflow"
lead: "Naming in Bizzflow is generally restricted by Airflow naming limitations."
date: 2022-06-20T08:24:57+02:00
lastmod: 2022-06-20T08:24:57+02:00
draft: false
images: []
menu:
  docs:
    parent: "project-reference"
    identifier: "naming-reference"
weight: 30
toc: true
---

Naming rules in Bizzflow are restricted by Airflow naming limitations. Airflow DAG names have maximum of 50 characters.
This limits the name lengths in Bizzflow.

For example every transformation DAG in Bizzflow starts with "40_Transformation_", that is 18 characters. That leaves
up to
32 (50 - 18 = 32) characters for your for transformation `id` in `transformations.json` . For example
"id": "an_id_that_is_32_characters_long".

Second naming example is extractor. Extractor DAG name consists of "20_Extractor_", that is 13 characters.
Next in is the `"type"` for example `ex-mysql_`, that is 9 characters. That leaves up to
28 (50 - 13 - 9 = 28) characters for your extractor file base-name. For example  "base_name_28_characters_long.json".
