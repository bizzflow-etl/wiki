---
title: "Writers"
description: >
  Writers are another way of getting your data out from Bizzflow. They are a component within your data pipeline
  that takes data from the output stage and writes them to some external system (such as a database or a CRM).
lead: >
  Writers are another way of getting your data out from Bizzflow. They are a component within your data pipeline
  that takes data from the output stage and writes them to some external system (such as a database or a CRM).
date: 2021-05-20T08:24:57+02:00
lastmod: 2021-05-20T08:24:57+02:00
draft: false
images: []
menu:
  docs:
    parent: "project-reference"
    identifier: "writers-reference"
weight: 55
toc: true
---

As opposed to [Datamarts]({{<ref "docs/project-reference/datamarts">}}), the direction is reversed -
with datamarts external systems connect to your warehouse. With writers, you actually send the data to an external
system.

## Writers Configuration

The configuration is very similar to [Extractors]({{<ref "docs/project-reference/extractors">}}). Each `JSON` or
`YAML` file located in `/writers/` directory is considered to be a configuration for some writer component.

You can find list of supported output systems [here](https://gitlab.com/bizzflow-writers).

Same as with extractors, each writer configuration is limited by
[naming]({{<ref "docs/project-reference/naming">}}) rules and
must include `type`, specifying which component Bizzflow
should use for writing data. Additionally, a writer should also receive input (same as
[Transformations]({{<ref "docs/project-reference/transformations">}})) using `input_tables` and `input_kexes` keys.

## Custom Writer Component Configuration

**If you want to use your own instead of Bizzflow's public
components** see [Component configuration]({{<ref "docs/project-reference/component-configuration">}}).

## Example: A simple writer configuration

Let's use a dummy writer component to pretend to be writing data from output kex `out_main` to outside of Bizzflow.

### `/writers/dummy.json`

```json
{
  "type": "wr-dummy",
  "input_kexes": ["out_main"],
  "config": {
    // dummy writer does not expect any configuration
  }
}
```

Or the same configuration in `YAML` format:

### `/writers/dummy.yaml`

```yaml
type: wr-dummy
input_kexes:
  - out_main
config:
```

Upon running the component, all tables from kex `out_main` will be passed to the writer.
