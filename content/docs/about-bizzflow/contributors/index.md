---
title: "Contributors"
description: "These are the contributors to the source code, ideas and concept of Bizzflow."
lead: "These are the contributors to the source code, ideas and concept of Bizzflow."
date: 2021-05-20T09:47:26+02:00
lastmod: 2021-05-20T09:47:26+02:00
draft: false
images: []
menu:
  docs:
    parent: "about-bizzflow"
weight: 10
toc: true
---

BizzTreat s.r.o., Prague, Czech Republic, [www.bizztreat.com](https://www.bizztreat.com)
