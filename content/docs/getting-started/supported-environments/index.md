---
title: "Supported Environments"
description: "You may find possible Cloud Providers and Data Warehouses combinations in the table below to find what suits you the most."
lead: "You may find possible Cloud Providers and Data Warehouses combinations in the table below to find what suits you the most."
date: 2021-05-15T13:45:34+02:00
lastmod: 2021-05-15T13:45:34+02:00
draft: false
images: []
menu:
  docs:
    parent: "getting-started"
weight: 30
toc: false
---

## Combinations

{{<combos>}}
