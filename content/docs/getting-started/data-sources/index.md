---
title: "Data Sources"
description: >
  Bizzflow can be used to connect to lots of various data source. Thanks to using Docker containers we also
  provide an easy interface for anyone who would like to extend Bizzflow and create a connector for a data source
  that is not supported yet.
lead: >
  Bizzflow can be used to connect to lots of various data source. Thanks to using Docker containers we also
  provide an easy interface for anyone who would like to extend Bizzflow and create a connector for a data source
  that is not supported yet.
date: 2021-05-21T11:31:42+02:00
lastmod: 2021-05-21T11:31:42+02:00
draft: false
images: []
menu:
  docs:
    parent: "getting-started"
weight: 40
toc: false
---

## List of officially available data sources

There are some data sources that we provide you with to get you started, however, it is not the full list of data sources that we have at our disposal. We also have a number of writers, too. Majority of these components is provided to our clients that own the licensed version of Bizzflow. If you would like to know more about licensed Bizzflow, inquire about a data source, or have us develop a custom component for you, please, reach out to us at [info@bizztreat.com](info@bizztreat.com). 

The list of of data sources, that we provide for free, is the following:

{{<data-sources>}}
