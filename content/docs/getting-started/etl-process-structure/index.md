---
title: "ETL Process Structure"
description: "Bizzflow comes with the best practices of Data Quality Assurance built in its core. In this chapter, we will cover the basics of the warehouse's structure."
lead: "Bizzflow comes with the best practices of Data Quality Assurance built in its core. In this chapter, we will cover the basics of the warehouse's structure."
date: 2021-05-15T16:30:13+02:00
lastmod: 2021-05-15T16:30:13+02:00
draft: false
images: []
menu:
  docs:
    parent: "getting-started"
weight: 20
toc: true
---

## Warehouse Structure

It doesn't matter which of the [supported warehouses]({{<ref "supported-environments" >}}) you use, Bizzflow
always comes with the same structure. Because the terminology differs amongst various warehouses, we came up
with few terms of our own in order to unambiguously label the parts of the structure.

[{{<img src="warehouse-structure.jpg" alt="Bizzflow Warehouse Structure" class="border-0" caption="Bizzflow Warehouse Structure">}}](/images/warehouse-structure.jpg)

What you see in the picture right above is what a small-scale Bizzflow project may look like
from the perspective of the warehouse.

You cat see we've got multiple data sources, then some magic happens and then we have
a 3<sup>rd</sup> party visualisation tool and a target system using our data. But what
does happen in between?

## Stages

When you decide to consolidate data from different data sources, they will always come
in different shape and data quality. Well, let's face it, they will be mostly **dirty**.
There will be values missing from your spreadsheet's cells, `null` datetimes in your
database's tables and your proprietary system's API only returns last month's data.

### raw

All this mess ends up being tables in the `raw` stage. **`raw` stage always contains
the data in the exact state and for as they came from the primary data source**. This way
you can always take a look at what the data looked like before all processing within
Bizzflow project. This is crucial when investigating errors in your data pipeline.

### in

You can deal with your API's behaviour using Bizzflow's [Step]({{<ref "/docs/project-reference/step">}}).
Thanks to `Step`'s increment configuration you may download last month's data and every
single increment of this data will be present in the `raw` stage, while the `in` stage
will contain the complete data for all of the history. **`in` stage always contains
the data that are ready to be used in transformations**.

[{{<img src="incremental-snapshotting.jpg" alt="Incremental snapshotting" class="border-0" caption="Incremental snapshotting">}}](/images/incremental-snapshotting.jpg)

<!-- {{<alert icon="❗">}}
Using **Step** is considered an advanced usage and will be thoroughly explained in
[Advanced Guide](ref "/docs/advanced-tutorial").
{{</alert>}} -->

### tr

Not exactly a stage, because `transformation` or `tr` stage is transient, meaning it only
exists temporarily. When you want to transform data from the `in` stage, Bizzflow
creates a copy of them in a temporary stage, executes all `SQL` scripts of your
transformation and outputs any table, that has `out_` prefix to `out` stage.

### out

**Output stage labels the data in it as processed data**, meaning they are to be further
used in production.

### dm

Datamart stage provides an additional way to distinguish between different kinds of output data.
Imagine you are processing all your company's data from all data sources. You run them
through various transformations using multiple `SQL` scripts and you create outputs based
on your company's departments. Everything works well when separated like this for the purpose
of your Tableau repoting. But for the sake of your sales' internal CRM system, you need some
of your marketing's data. Mixing all of your sales' and all of your marketing's data is insecure.
You can create separate datamart for this specific use case and make
sure you don't leak all of your marketing's data and provide them to the sales.
