---
title: "Foreword"
description: >
  If you encounter any problems during installation or Bizzflow usage, this section should be your
  first destination. You will find here most commonly encountered issues and possible
  ways of resolving them. If you haven't found what you were looking for, consider
  creating an <a href="https://gitlab.com/bizzflow-etl/toolkit/-/issues">issue
  in the Bizzflow repository</a>.
lead: >
  If you encounter any problems during installation or Bizzflow usage, this section should be your
  first destination. You will find here most commonly encountered issues and possible
  ways of resolving them. If you haven't found what you were looking for, consider
  creating an <a href="https://gitlab.com/bizzflow-etl/toolkit/-/issues">issue
  in the Bizzflow repository</a>.
date: 2021-05-19T23:16:27+02:00
lastmod: 2021-05-19T23:16:27+02:00
draft: false
images: []
menu:
  docs:
    parent: "troubleshooting"
weight: 10
toc: true
---
