---
title: "ETL for everyone"
description: "Bizzflow is your private ETL solution within your cloud provider's project container."
lead: "Bizzflow is your private ETL solution within your cloud provider's project container."
date: 2020-10-06T08:47:36+00:00
lastmod: 2020-10-06T08:47:36+00:00
draft: false
images: []
---
