---
title: "toolkit 1.1.6"
description: "Toolkit version 1.1.6"
lead: "Toolkit version 1.1.6"
date: 2021-08-13T12:08:22.000+02:00
lastmod: 2021-08-13T12:08:22.000+02:00
draft: false
weight: 50
images: []
---

## Bugfixes

- fix csv unload from Azure SQL to Azure BLOB Storage
