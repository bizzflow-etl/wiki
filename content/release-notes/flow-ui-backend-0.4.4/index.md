---
title: "backend 0.4.4"
description: "Flow UI Backend 0.4.4"
lead: "Flow UI Backend 0.4.4"
date: 2023-02-23T21:23:33+00:00
lastmod: 2023-02-23T21:23:33+00:00
draft: false
weight: 50
images: []
---

- use toolkit 2.4.0
- login to Flow UI from Airflow
