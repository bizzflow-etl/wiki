---
title: "toolkit 1.1.0"
description: "Toolkit version 1.1.0"
lead: "Toolkit version 1.1.0"
date: 2021-06-24T20:19:19+02:00
lastmod: 2021-06-24T20:19:19+02:00
draft: false
weight: 50
images: []
---

## Bugfixes

- users' naming in Google Cloud Platform

## New features

- prefer `user_exists` over listing users in credential managers
- refresh user's permissions to transformation kex on each run
- enable listing for `VaultManager`

## Additional information

- major refactoring of credentials managers
- transformations now use `CredentialsManager` along with `SandboxManager` to reduce code repetition
