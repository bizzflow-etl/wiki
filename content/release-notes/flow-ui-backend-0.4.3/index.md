---
title: "backend 0.4.3"
description: "Flow UI Backend 0.4.3 release"
lead: "Flow UI Backend 0.4.3 release"
date: 2023-01-12T15:00:50+00:00
lastmod: 2023-01-12T15:00:50+00:00
draft: false
weight: 50
images: []
---

## Features

- Use `toolkit 2.3.0`.
- Bugfix for list of transformations not loading.
- Bugfix for Flow UI storage table details not loading.
