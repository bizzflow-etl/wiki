---
title: "toolkit 1.0.1"
description: "Toolkit version 1.0.1"
lead: "Toolkit version 1.0.1"
date: 2021-06-21T09:20:41+02:00
lastmod: 2021-06-21T09:20:41+02:00
draft: false
weight: 50
images: []
---

## Bugfixes

- upload files as bytes to `Azure BLOB Storage` to work around hard-coded `iso-8859-1` encoding in official Azure module
- ensure VM is running before retrying operations
- workaround for `path.exists` not working for symlinks
- create symlinks for all DAG files
- non-select queries should not try to return values
- setup secret key for Airflow in installations with default settings
- slugify docker image tags

## New features

- retry `git pull` and `git clone` operations
- replace credentials in nested arrays
- allow running transformations in parallel
- simplified table preview on Azure SQL backend

Lots of smaller changes and bugfixes. See commit messages for more.
