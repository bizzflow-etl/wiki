---
title: "backend 0.2.0"
description: "Flow UI Backend 0.2.0 release"
lead: "Flow UI Backend 0.2.0 release"
date: 2021-12-07T13:26:18+00:00
lastmod: 2021-12-07T13:26:18+00:00
draft: false
weight: 50
images: []
---

## New features

- Added basic storage operations (delete table, copy table, truncate table, delete kex).
- Some storage operations now run synchronously (UI will wait and display results immediately).
- Storage can now also be refreshed partially.
- Credentials' type can now be inferred using API rather than by the name.

## Bugfixes

- `poetry` installs dependencies using `poetry.lock` file which shortened the build time considerably.

## Additional information

- `toolkit` was bumped to `1.2.0`
