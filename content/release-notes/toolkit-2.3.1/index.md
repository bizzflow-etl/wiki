---
title: "toolkit 2.3.1"
description: "Bizzflow toolkit 2.3.1 release"
lead: "Bizzflow toolkit 2.3.1 release"
date: 2023-02-06T15:25:00+01:00
lastmod: 2023-02-06T15:25:00+01:00
draft: false
weight: 50
images: []
---

## Bugfixes

- notifications will not be triggered from dag_status and ignore_leaf tasks
