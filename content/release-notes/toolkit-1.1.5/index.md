---
title: "toolkit 1.1.5"
description: "Toolkit version 1.1.5"
lead: "Toolkit version 1.1.5"
date: 2021-08-12T10:44:28.000+02:00
lastmod: 2021-08-12T10:44:28.000+02:00
draft: false
weight: 50
images: []
---

## Bugfixes

- fix user naming in GCP (dashes were used instead of underscores)
- grant readonly access to a datamart schema in Azure SQL
