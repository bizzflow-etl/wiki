---
title: "toolkit 1.1.1"
description: "Toolkit version 1.1.1"
lead: "Toolkit version 1.1.1"
date: 2021-06-28T09:51:09.000+02:00
lastmod: 2021-06-28T09:51:09.000+02:00
draft: false
weight: 50
images: []
---

## Bugfixes

- work around strange bug when trying to install specific azure blob version

## New features

- on each commit to `master`, a Docker with `:latest` tag will be created
