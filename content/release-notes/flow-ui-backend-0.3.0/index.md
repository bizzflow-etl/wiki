---
title: "backend 0.3.0"
description: "Flow UI Backend 0.3.0"
lead: "Flow UI Backend 0.3.0"
date: 2022-06-06T14:39:45+00:00
lastmod: 2022-06-06T14:39:45+00:00
draft: false
weight: 50
images: []
---

- Bizzflow on-prem support
- Toolkit version 2.1.0
