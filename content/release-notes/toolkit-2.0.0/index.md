---
title: "toolkit 2.0.0"
description: ""
lead: ""
date: 2022-03-16T07:25:15+00:00
lastmod: 2022-03-16T07:25:15+00:00
draft: false
weight: 50
images: []
---

# New features

- major refactoring - now no coponent have looking for configuration to ConfigurationManager but otherwise ConfigurationManager can create all other managers and executors based on config for you

## Bugfixes

- fix incremental load, there was an issue when comparing NULL values
- load custom all DAGs from project repository correctly
- fix local source component build on Azure
- multiple docker component with same source can now run in parallel

## Other

- Configurationn V2 draft
