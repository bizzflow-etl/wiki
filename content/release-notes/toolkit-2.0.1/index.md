---
title: "toolkit 2.0.1"
description: ""
lead: ""
date: 2022-03-24T15:10:15+01:00
lastmod: 2022-03-24T15:10:15+01:00
draft: false
weight: 50
images: []
---

## Bufixes

- allow WITH in sql query
- delete last_operation column and used new __last_operation instead 
- fix notify settings for orchestration and set it as default True as it was
- fix to strict validaiton for out_kex in transforamtion
