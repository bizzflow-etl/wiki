---
title: "toolkit 2.2.2"
description: "Bizzflow Toolkit 2.2.2 released"
lead: "Bizzflow Toolkit 2.2.2 released"
date: 2022-06-22T17:20:54+02:00
lastmod: 2022-06-22T17:20:54+02:00
draft: false
weight: 50
images: []
---

## Bugfixes
- Temporary fix for sharing tables in BQ - until full sharing support will be ready
