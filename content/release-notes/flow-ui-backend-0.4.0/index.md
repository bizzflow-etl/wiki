---
title: "backend 0.4.0"
description: "Flow UI Backend 0.4.0"
lead: "Flow UI Backend 0.4.0"
date: 2022-06-22T11:12:28+02:00
lastmod: 2022-06-22T11:12:28+02:00
draft: false
weight: 50
images: []
---

- Add sharing credentials support
- Add postgresql reserved credentials
