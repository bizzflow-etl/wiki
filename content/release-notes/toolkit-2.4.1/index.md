---
title: "toolkit 2.4.1"
description: "2.4.1 release"
lead: "2.4.1 release"
date: 2023-03-09T18:26:44+01:00
lastmod: 2023-03-09T18:26:44+01:00
draft: false
weight: 50
images: []
---

## Features

- Allow selecting SQL parser for transformation SQL files
