---
title: "toolkit 1.2.1"
description: "Toolkit 1.2.1 release"
lead: "Toolkit 1.2.1 release"
date: 2021-12-21T16:39:30+00:00
lastmod: 2021-12-21T16:39:30+00:00
draft: false
weight: 50
images: []
---

## Features

- Not as strict anymore for input kexes - does not require prefix `_in` or `_out` - just exclude `raw_` and `_dm`
