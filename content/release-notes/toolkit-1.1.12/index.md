---
title: "toolkit 1.1.12"
description: "Toolkit version 1.1.12"
lead: "Toolkit version 1.1.12"
date: 2021-09-29T11:43:21.000+00:00
lastmod: 2021-09-29T11:43:21.000+00:00
draft: false
weight: 50
images: []
---

## Bugfixes

- fix Snowflake roles dropping
- set reserved columns on incremental insert on Snowflake
