---
title: "frontend 0.2.1"
description: ""
lead: ""
date: 2022-03-15T16:30:57+00:00
lastmod: 2022-03-15T16:30:57+00:00
draft: false
weight: 50
images: []
---

## Bugfixes

* fix CORS error in vault when adding new credentials
