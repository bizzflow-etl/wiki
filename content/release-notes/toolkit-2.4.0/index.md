---
title: "toolkit 2.4.0"
description: "2.4.0 release"
lead: "2.4.0 release"
date: 2023-02-23T20:54:14+00:00
lastmod: 2023-02-23T20:54:14+00:00
draft: false
weight: 50
images: []
---

## Warning

This release requires that you migrate Airflow metadata DB. See
[migration.md](https://gitlab.com/bizzflow-etl/toolkit/-/blob/master/migration.md) for details.

## Features

- updated to Python 3.10
- updated to Airflow 2.5

## Bugfixes

- `UNION` in `step.json` now works correctly for PostgreSQL
- notifications are not sent twice anymore
- in some cases, temporary credentials were listed in logs, this is now fixed
