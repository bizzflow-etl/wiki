---
title: "toolkit 1.0.0"
description: "Toolkit version 1.0.0"
lead: "Toolkit version 1.0.0"
date: 2021-05-18T13:17:26+00:00
lastmod: 2021-05-18T13:17:26+00:00
draft: false
weight: 50
images: []
---

## Bugfixes

- `pyodbc` imports should not fail on non-AzureSQL installations anymore
- assume orchestrator role for tables listing in AzureSQL
- identation bug causing failure of empty transformations
- do not list files in subdirectories when using `FileStorageManager`

## New features

- `transformation_service_account` is allowed to be empty in `transformations.json` to make different Bizzflow backend
  versions more compatible
- run all containers with `--rm` flag to minimize amount of leftover from Docker containers
- multiple tweaks to make installation easier
