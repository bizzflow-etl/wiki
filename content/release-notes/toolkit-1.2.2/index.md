---
title: "toolkit 1.2.2"
description: "Toolkit 1.2.2 released"
lead: "Toolkit 1.2.2 released"
date: 2022-02-08T15:51:30+00:00
lastmod: 2022-02-08T15:51:30+00:00
draft: false
weight: 50
images: []
---

## Bugfixes

- fix `Datasource already exists` error (could sometimes occur in Azure)
- typo in `bizzflow-init` that caused setup app to fail when creating Azure BLOB Storage credentials

## New features

- both escaping variants (`"tr"` and `[tr]`) now work on Azure SQL transformation queries
