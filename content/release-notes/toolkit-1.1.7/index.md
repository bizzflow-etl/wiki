---
title: "toolkit 1.1.7"
description: "Toolkit version 1.1.7"
lead: "Toolkit version 1.1.7"
date: 2021-08-17T09:16:08.000+02:00
lastmod: 2021-08-17T09:16:08.000+02:00
draft: false
weight: 50
images: []
---

## Bugfixes

- credentials load for BigQuery transformations
