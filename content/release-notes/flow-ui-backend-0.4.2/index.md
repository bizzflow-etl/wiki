---
title: "backend 0.4.2"
description: "Flow UI Backend release 0.4.2"
lead: "Flow UI Backend release 0.4.2"
date: 2022-06-28T13:28:04+00:00
lastmod: 2022-06-28T13:28:04+00:00
draft: false
weight: 50
images: []
---

- rebuild for latest toolkit (2.2.2)
