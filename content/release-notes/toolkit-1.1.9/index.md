---
title: "toolkit 1.1.9"
description: "Toolkit version 1.1.9"
lead: "Toolkit version 1.1.9"
date: 2021-09-13T16:45:42.000+02:00
lastmod: 2021-09-13T16:45:42.000+02:00
draft: false
weight: 50
images: []
---

## New Features

- catch timeout error on every ssh command
- keep-alive session settings for Snowflake

## Additional information

- global notification e-mail is no longer added to legacy notifications
