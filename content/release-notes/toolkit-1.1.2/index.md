---
title: "toolkit 1.1.2"
description: "Toolkit version 1.1.2"
lead: "Toolkit version 1.1.2"
date: 2021-08-04T11:15:15.000+02:00
lastmod: 2021-08-04T11:15:15.000+02:00
draft: false
weight: 50
images: []
---

## Bugfixes

- work around strange bug when trying to install specific azure blob version
- clean csv output from Snowflake
- read-only access for Snowflake "sandbox" Kexes

## New features

- allow project/database prefix in `input_kexes` for transformations
- export data from Snowflake as a single csv
- read uncommitted transaction isolation level for Azure SQL backend
- load custom DAGs from project

## Additional information

- removed `pyodbc` dependency (it was causing troubles and is only needed for Azure SQL installations)
