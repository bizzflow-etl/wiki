---
title: "frontend 0.2.0"
description: "Flow UI frontend 0.2.0 released."
lead: "Flow UI frontend 0.2.0 released."
date: 2021-12-07T14:01:47+00:00
lastmod: 2021-12-07T14:01:47+00:00
draft: false
weight: 50
images: []
---

## New features

- Synchronous storage operations.
- Vault is now separated into three sections based on credentials type.
- Added support for creating and editing credentials in vault.
- Implemented backoff for backend retries.
- Vault will now display credentials' id to make it easier to use them in configurations.
- Credentials' type is now inferred using backend API.
- `ProgressSpinner` was separated into individual component.
- Partial refresh of the storage tree is now available.
- Table preview was added.
- Added `Back to Airflow` button into user dropdown.

## Bugfixes

- Few graphical glitches were fixed.
- Alerts will no more be displayed duplicitly on repeated errors.

## Additional information

- Built for `backend 0.2.0` with `toolkit 1.2.0`
- Beta warning can now be hidden.
