---
title: "backend 0.1.4"
description: "Flow UI backend version 0.1.4"
lead: "Flow UI backend version 0.1.4"
date: 2021-10-08T13:42:57.000+00:00
lastmod: 2021-10-08T13:42:57.000+00:00
draft: false
weight: 50
images: []
---

## Bugfixes

- rollback on failed transactions

## New features

- use Airflow's `Session` as context manager
