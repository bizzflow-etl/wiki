---
title: "toolkit 1.1.14"
description: "Toolkit version 1.1.14"
lead: "Toolkit version 1.1.14"
date: 2021-10-05T10:55:29.000+02:00
lastmod: 2021-10-05T10:55:29.000+02:00
draft: false
weight: 50
images: []
---

## Bugfixes

- return empty table details rather than `None` when no table details are found by `StorageManager`
