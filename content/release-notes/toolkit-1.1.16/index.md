---
title: "toolkit 1.1.16"
description: "Toolkit version `1.1.16`."
lead: "Toolkit version `1.1.16`."
date: 2021-11-08T12:26:53+00:00
lastmod: 2021-11-08T12:26:53+00:00
draft: false
weight: 50
images: []
---

## Bugfixes

- It is possible to unload table bigger then 5GB from snowflake to worker - it's separated to multiple files and gzipped and after upload to worker these chunks are merged in one files, so it's faster and thrasfer less date (because of compression)
- `.git` suffix is properly added to repo url when cloning a component from git repository
- random Azure SQL password will always be compliant with Azure SQL Password Policies

## New Features

- Attempt to run remote command on worker machine will retry on Paramiko's `EOFError`
- All new releases will automatically generate a post for [Bizzflow's Wiki](https://wiki.bizzflow.net/release-notes/)

## Additional information

- bizzflow will no longer attempt to install `unzip` in worker machine if it's not present
