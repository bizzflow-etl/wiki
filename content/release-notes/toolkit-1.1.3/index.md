---
title: "toolkit 1.1.3"
description: "Toolkit version 1.1.3"
lead: "Toolkit version 1.1.3"
date: 2021-08-10T12:54:19.000+02:00
lastmod: 2021-08-10T12:54:19.000+02:00
draft: false
weight: 50
images: []
---

## Bugfixes

- respect Azure BLOB Storage's max chunk size of 4MB
