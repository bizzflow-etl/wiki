---
title: "toolkit 2.1.1"
description: "Bizzflow Toolkit 2.1.1 released"
lead: "Bizzflow Toolkit 2.1.1 released"
date: 2022-05-26T16:05:02+02:00
lastmod: 2022-05-26T16:05:02+02:00
draft: false
weight: 50
images: []
---

## New Features

- allow multipart table export to GCS from BigQuery to support bigger tables export

## Bugfixes

- do not require sharing config file
- fix export tables to the worker for snowflake and BigQuery
