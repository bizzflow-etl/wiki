---
title: "frontend 0.3.0"
description: "Flow UI frontend 0.3.0"
lead: "Flow UI frontend 0.3.0"
date: 2022-06-22T11:09:38+02:00
lastmod: 2022-06-22T11:09:38+02:00
draft: false
weight: 50
images: []
---

- Add sharing credentials support
- Add postgresql reserved credentials
