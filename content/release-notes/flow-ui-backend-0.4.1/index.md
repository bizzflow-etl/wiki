---
title: "backend 0.4.1"
description: "Flow UI backend 0.4.1"
lead: "Flow UI backend 0.4.1"
date: 2022-06-28T13:28:04+00:00
lastmod: 2022-06-28T13:28:04+00:00
draft: false
weight: 50
images: []
---

- fix argument order to make backend work with the latest toolkit
- always use latest toolkit when building
