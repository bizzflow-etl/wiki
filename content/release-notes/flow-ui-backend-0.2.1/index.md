---
title: "backend 0.2.1"
description: "Flow UI Backend 0.2.1 containing fix for 0.2.0 released."
lead: "Flow UI Backend 0.2.1 containing fix for 0.2.0 released."
date: 2021-12-09T11:06:45+00:00
lastmod: 2021-12-09T11:06:45+00:00
draft: false
weight: 50
images: []
---

## Bugfixes

- fix dependency resolution wrongly installing `wtforms==3.0.0` which is incompatible with Airflow's Flask plugins

## Additional information

- a dev `docker-compose` was added to the repository to make development less painful
