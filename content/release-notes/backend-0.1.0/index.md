---
title: "backend 0.1.0"
description: "Flow UI backend version 0.1.0"
lead: "Flow UI backend version 0.1.0"
date: 2021-06-24T21:09:35.000+02:00
lastmod: 2021-06-24T21:09:35.000+02:00
draft: false
weight: 50
images: []
---

First release. Please note that this release of Flow UI is still in beta.
