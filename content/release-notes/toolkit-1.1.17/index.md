---
title: "toolkit 1.1.17"
description: "Version 1.1.17 of toolkit released."
lead: "Version 1.1.17 of toolkit released."
date: 2021-11-29T09:00:52+00:00
lastmod: 2021-11-29T09:00:52+00:00
draft: false
weight: 50
images: []
---

## Bugfixes
 - fix loading input tables to docker transformation on Azure

## New features

- show toolkit version on airflow logs

## Additional information
