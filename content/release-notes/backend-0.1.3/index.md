---
title: "backend 0.1.3"
description: "Flow UI backend version 0.1.3"
lead: "Flow UI backend version 0.1.3"
date: 2021-10-05T16:11:00.000+02:00
lastmod: 2021-10-05T16:11:00.000+02:00
draft: false
weight: 50
images: []
---

## Bugfixes

- verify `jwt` signatures for Google IAP headers using latest keys

## New features

- health check for compose services

## Additional information

- always curl `master` docker-compose
- bump `toolkit` version to `1.1.12`
