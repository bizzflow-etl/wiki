---
title: "toolkit 2.1.2"
description: "Bizzflow Toolkit 2.1.2 released"
lead: "Bizzflow Toolkit 2.1.2 released"
date: 2022-05-31T09:58:37+02:00
lastmod: 2022-05-31T09:58:37+02:00
draft: false
weight: 50
images: []
---

## New Features

- show execution time for individual parts of the job in logs 

## Bugfixes

- fix naming of exported input mapping files to docker executor - remove `in_` prefix as redundant
