---
title: "toolkit 1.0.2"
description: "Toolkit version 1.0.2"
lead: "Toolkit version 1.0.2"
date: 2021-06-23T15:21:52+02:00
lastmod: 2021-06-23T15:21:52+02:00
draft: false
weight: 50
images: []
---

## Bugfixes

- fix `Azure BLOB Storage` files listing to include subdirectories only when requested
