---
title: "toolkit 2.2.1"
description: "Bizzflow Toolkit 2.2.1 released"
lead: "Bizzflow Toolkit 2.2.1 released"
date: 2022-06-22T16:43:28+02:00
lastmod: 2022-06-22T16:43:28+02:00
draft: false
weight: 50
images: []
---

## Bugfixes

- fix increment load from version 2.2.0
