---
title: "toolkit 1.1.13"
description: "Toolkit version 1.1.13"
lead: "Toolkit version 1.1.13"
date: 2021-09-30T10:07:09.000+02:00
lastmod: 2021-09-30T10:07:09.000+02:00
draft: false
weight: 50
images: []
---

## Bugfixes

- set encoding for outbound notifications explicitly to `utf8` in order to make non-ascii characters work
- fix for Sandbox loading when there were either no additional kexes or no additional tables set
- `curl` base repository using `https` instead of `git` cloning via `ssh` to make it work even when no ssh key is present
- push installation changes to project repository without branch specification (to allow for any default branch name)
- fix for Sandbox loading failure in some rare cases when no transformation was specified

## Additional information

- project update is no longer done with `clone repo -> delete original -> replace with newest` but rather
  `pull latest -> roll back on error` to make sure the project Docker volume is not deleted on the fly
