---
title: "toolkit 2.4.2"
description: "Toolkit 2.4.2 release"
lead: "Toolkit 2.4.2 release"
date: 2023-04-12T16:45:48+02:00
lastmod: 2023-04-12T16:45:48+02:00
draft: false
weight: 50
images: []
---

## Bugfixes

- Fix `get_queries` argument that blocked transformations from running.
