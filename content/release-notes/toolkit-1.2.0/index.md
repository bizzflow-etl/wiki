---
title: "toolkit 1.2.0"
description: "Version 1.2.0 of toolkit released."
lead: "Version 1.2.0 of toolkit released."
date: 2021-12-07T10:50:53+01:00
lastmod: 2021-12-07T10:50:53+01:00
draft: false
weight: 50
images: []
---

## New features

- allow to set custom component (extractor, writer, docker transformation) from multiple sources as custom git, docker registry or local folder
- allow storing metadata for tables and kexes
