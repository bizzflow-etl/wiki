---
title: "toolkit 2.2.0"
description: ""
lead: ""
date: 2022-06-22T10:03:47+02:00
lastmod: 2022-06-22T10:03:47+02:00
draft: false
weight: 50
images: []
---

## New Features

- sharing tables in Azure SQL - it's possible to use tables from different DB as an external source and otherwise share tables from the current project
- allow setting ExecutionTimeout for tasks

## Bugfixes

- fix input mapping - allow 'input_kexes' argument in config
- fix the local component source path - use the absolute path
