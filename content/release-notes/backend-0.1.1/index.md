---
title: "backend 0.1.1"
description: "Flow UI backend version 0.1.1"
lead: "Flow UI backend version 0.1.1"
date: 2021-06-28T21:10:54.000+02:00
lastmod: 2021-06-28T21:10:54.000+02:00
draft: false
weight: 50
images: []
---

## Bugfixes

- make `docker-compose` work for older projects

## New features

- run Flow UI as root for older projects
- add `:latest` tag for builds on commits to `master`
