---
title: "frontend 0.4.0"
description: "Flow UI Frontend 0.4.0"
lead: "Flow UI Frontend 0.4.0"
date: 2023-02-23T21:26:51+00:00
lastmod: 2023-02-23T21:26:51+00:00
draft: false
weight: 50
images: []
---

* purge unused CSS
* lazy load for js modules
* minor changes
