---
title: "frontend 0.1.0"
description: "Flow UI Frontend version 0.1.0"
lead: "Flow UI Frontend version 0.1.0"
date: 2021-06-24T17:17:55.000+02:00
lastmod: 2021-06-24T17:17:55.000+02:00
draft: false
weight: 50
images: []
---

First release. Please note that this is a beta release of Flow UI.
