---
title: "toolkit 1.1.4"
description: "Toolkit version 1.1.4"
lead: "Toolkit version 1.1.4"
date: 2021-08-10T17:26:27.000+02:00
lastmod: 2021-08-10T17:26:27.000+02:00
draft: false
weight: 50
images: []
---

## Bugfixes

- fix high memory usage caused by using [`itertools.tee`](https://docs.python.org/3/library/itertools.html#itertools.tee)
