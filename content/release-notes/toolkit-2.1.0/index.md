---
title: "toolkit 2.1.0"
description: "Bizzflow Toolkit 2.1.0 released"
lead: "Bizzflow Toolkit 2.1.0 released"
date: 2022-05-20T19:07:17+02:00
lastmod: 2022-05-20T19:07:17+02:00
draft: false
weight: 50
images: []
---

## New Features

- Bizzflow can run locally with PostgreSQL as a storage
- Allow sharing table from and to another DB - azure SQL only
