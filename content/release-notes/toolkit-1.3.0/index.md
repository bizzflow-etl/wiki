---
title: "toolkit 1.3.0"
description: "Bizzflow Toolkit 1.3.0 released"
lead: "Bizzflow Toolkit 1.3.0 released"
date: 2022-02-21T14:06:22+00:00
lastmod: 2022-02-21T14:06:22+00:00
draft: false
weight: 50
images: []
---

## Bugfixes

- Fix Azure SQL incremental `MERGE` query
- load custom DAGs from project repository correctly

## New Features

- allow running `WITH` clause queries in SQL transformations
- add more tracking information to e-mail error notification
