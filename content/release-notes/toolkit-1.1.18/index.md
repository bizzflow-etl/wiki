---
title: "toolkit 1.1.18"
description: "Toolkit release 1.1.18"
lead: "Toolkit release 1.1.18"
date: 2021-12-06T16:09:17+00:00
lastmod: 2021-12-06T16:09:17+00:00
draft: false
weight: 50
images: []
---

## Bugfixes

- Added retry when trying to establish connection to Azure SQL - this should prevent "Login timeout expired" or even "Database is not currently available" 
- Fix azure docker transformation - invalid file naming from version `1.1.17` and `1.1.16`
