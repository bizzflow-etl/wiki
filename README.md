# Bizzflow wiki

## How to develop

### 1. Download source

```console
git clone git@gitlab.com:bizztreat/bizzflow/r2/wiki.git
```

### 2. Install requirements

```console
yarn install
```

### 3. Run development server

```console
yarn start
```

### 4. Edit the wiki

#### Basics

Markdown files are located in `/content/` directory. Every folder that contains `_index.md` file is a section.
Every folder within a section that contains `index.md` file is a page in the section.

#### Edit menu

Menus are configured in `menus.toml` file. Please note that sections and pages need to have appropriate identifiers
set within their front matter.
